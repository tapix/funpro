(ns test-lein.core
  (:gen-class))

(fn [name] (str "Tervetuloa Tylypahkaan " name))
(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (+ (* 2 5) 4))
  (println (+ 1 2 3 4 5)) 
  (println ((fn [name] (str "Tervetuloa Tylypahkaan " name)) "Tapio"))
  (println (get (get {:name {:first "Urho" :middle "Kaleva" :last "Kekkonen"}} :name) :middle)))
  