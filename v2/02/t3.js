'use strict';
const Immutable = require('immutable');


const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);
const set2 = set1.add(['"ruskea"']);
console.log(set1);
console.log(set2);
console.log(set1===set2);

set2.add(['"ruskea"']);
const set3 = Immutable.Set(set2);
console.log(set2);
console.log(set3);
console.log(set2===set3);
return;

//ensimmäisessä sisällöt eivät vastanneet toisiaan, eli: false
//set2 lisättiin ruskea, joten se ei ollut sama set1:n kanssa
//toisessa sisällöt vastasivat toisiaan, eli: true
//set2 ei voitu lisätä arvoa "ruskea" uudestaan, koska Set ei salli duplikaatteja