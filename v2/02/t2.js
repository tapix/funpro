'use strict';

const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    
    var tankki=0;
    var p_matkamittari=0;
    class Auto{
        constructor(){
            suojatut.set(this, {matkamittari: p_matkamittari});
        }  // suojatut instanssimuuttujat matkamittari
        
     
        getMatkamittari() {return suojatut.get(this).matkamittari;}
        increaseTankki() {
            tankki=tankki+100;
        }
        getTankki(){
            return tankki;
        }
        aja(){
            if(tankki>0){
                p_matkamittari=suojatut.get(this).matkamittari;
                suojatut.set(this, {matkamittari: p_matkamittari+1});
                tankki=tankki-10;
                console.log("Ajetaan");
            }else{
                console.log("Ei ajeta, tankkaa!!");
            }
            return 0;
        }
        getMap() {return suojatut;}  // testausta varten
        
    }
    
    return Auto;
})();

const auto1 = new Auto();
console.log("Ajettu: "+auto1.getMatkamittari()+"km");
console.log("Tankissa: "+auto1.getTankki()+"ml");
console.log("Tankataan 100ml...");
auto1.increaseTankki();
console.log("Ajettu: "+auto1.getMatkamittari()+"km");
console.log("Tankissa: "+auto1.getTankki()+"ml");
auto1.aja();
console.log("Ajettu: "+auto1.getMatkamittari()+"km");
console.log("Tankissa: "+auto1.getTankki()+"ml");
auto1.aja();
auto1.aja();
auto1.aja();
console.log("Ajettu: "+auto1.getMatkamittari()+"km");
console.log("Tankissa: "+auto1.getTankki()+"ml");
auto1.aja();
auto1.aja();
auto1.aja();
auto1.aja();
auto1.aja();
auto1.aja();
auto1.aja();
auto1.aja();
console.log("Ajettu: "+auto1.getMatkamittari()+"km");
console.log("Tankissa: "+auto1.getTankki()+"ml");
return 0;