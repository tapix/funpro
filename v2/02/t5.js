const Immutable = require('immutable');

var firstTenElements = Immutable.Repeat()
                                .map( Math.random )
                                .take( 10 )
                                .toJSON();

console.log(firstTenElements); 
console.log(firstTenElements); 
//Palauttaa loputtoman sequenssin annetusta arvosta, jos kertoja (times) ei olla määritetty
//Esimerkki tallettaa firstTenElementsiin 10 satunnaista lukua 
//JSON muodossa.
//
//verrattavissa for-looppiin?

//!!Tehtävä 5!!
var repeater = Immutable.Repeat("hello",4);
console.log(repeater);
console.log(repeater.toJSON());

var repeater = Immutable.Repeat("hello");
console.log(repeater);
console.log(repeater.take( 5 ).toJSON());


//tai esimerkkiä muunnellen, voi käyttäää satunnaisnumerogeneraattorina muualla ohjelmassa 
//jättämällä pelkästään .map(Math.random):in Repeattiin

var firstTenElements = Immutable.Repeat()
                                .map( Math.random );

console.log(firstTenElements.take( 2 ).toJSON()); 
console.log(firstTenElements.take( 2 ).toJSON()); 