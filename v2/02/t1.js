function laskePisteet(pituus, kPiste, lisapisteet){
    let points=0;
    if(pituus>=kPiste){
        points=points+60;
    }
   // console.log(pituus-kPiste);
    return points+((pituus-kPiste)*lisapisteet);
}

function initMaki(kPiste=0){
    let lisapisteet=0;
    if(kPiste>=75&&kPiste<=99){
        lisapisteet=2;
    }else if(kPiste>=100&&kPiste<=130){
        lisapisteet=1.8;
    }
    return function (pituus){
        return laskePisteet(pituus,kPiste,lisapisteet);
    };
}

//käyttäjän määriteltävä mäen kPiste
//lisapisteet lasketaan kPisteen mukaan
//käyttäjä ei saa määrittää lisapisteet arvoa, koska
//se on tehtävänannon mukaan jo valmiiksi määritelty
//(normaalimäki jos kPiste 75-99: lisapisteet=2
// suurmäki jos kPiste 100-130: lisapisteet=1.8)
var normaaliLahti = initMaki(90);
var suuriMaki = initMaki(130);

console.log("Normilahden kisat ohi! Pisteet:");
console.log("Kisaaja 1\t: "+normaaliLahti(98));
console.log("Kisaaja 2\t: "+normaaliLahti(130));
console.log("Kisaaja 3\t: "+normaaliLahti(131));
console.log("Kisaaja 4\t: "+normaaliLahti(78));
console.log("Kisaaja 5\t: "+normaaliLahti(110));
console.log("Kisaaja 6\t: "+normaaliLahti(105));
console.log("Kisaaja 7\t: "+normaaliLahti(50));
console.log("");

console.log("Suurilahden kisat ohi! Pisteet:");
console.log("Kisaaja 1\t: "+suuriMaki(98));
console.log("Kisaaja 2\t: "+suuriMaki(130));
console.log("Kisaaja 3\t: "+suuriMaki(131));
console.log("Kisaaja 4\t: "+suuriMaki(78));
console.log("Kisaaja 5\t: "+suuriMaki(110));
console.log("Kisaaja 6\t: "+suuriMaki(105));
console.log("Kisaaja 7\t: "+suuriMaki(50));
console.log("Kisaaja 8\t: "+suuriMaki(129));
return;