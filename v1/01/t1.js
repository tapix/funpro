function palindrome(n) {
//console.log("String:'"+n+"'");
  if (n.length === 0 || n.length === 1) {
    return 1;
  }else if(n[0].localeCompare(n[n.length-1])!==0){
    return 0;
  }else{
    //first and last are the same, check mid
    var b = n.substring(1);
    b = b.slice(0,-1);
    if(palindrome(b)===1){
      return 1;
    }else{
      return 0;
    }
  }
}

console.log("1:"+palindrome("a"));
console.log("1:"+palindrome(""));
console.log("1:"+palindrome("aa"));
console.log("1:"+palindrome("apa"));
console.log("0:"+palindrome("omena"));
console.log("1:"+palindrome("saippuakauppias"));
console.log("1:"+palindrome("bananab"));
console.log("0:"+palindrome("omenamo"));
console.log("0:"+palindrome("saapas"));
console.log("0:"+palindrome("meowies"));
console.log("0:"+palindrome("meow"));
console.log("1:"+palindrome("meem"));
console.log("1:"+palindrome("omenemo"));
console.log("1:"+palindrome("sokos"));
