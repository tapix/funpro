function flip(lista,a=0) {
  var temp;
  temp=lista[lista.length-1-a];
  lista[lista.length-1-a]=lista[a];
  lista[a]=temp;
  //console.log(a+":"+((lista.length)/2));
  if(a==Math.floor((lista.length-1)/2)||a===20){ //a===20 jos koodissa vikaa
    return lista;
  }else{
    return flip(lista,a+1);
  }
}
console.log("Flipped:"+flip([0,1,2,3,4,5,6,7,8,9])); 
console.log("Flipped:"+flip([0,1,2,3])); 
console.log("Flipped:"+flip([0,1,2,3,4])); 
console.log("Flipped:"+flip([0,1,2,3,4,5])); 
console.log("Flipped:"+flip([9,8,7,6,5,4,3,2,1,0])); 