        'use strict';
        
        let f, g;
        function foo() {
          let x;
          f = function() { return ++x; };
          g = function() { return --x; };
          x = 3;
         // console.log('inside foo, call to f(): ' + f());
        }
        foo();  
        console.log('call to g(): ' + g()); 
        console.log('call to f(): ' + f());  
        
        //Tässä tallennetaan kaksi funktiota f ja g ja niitä käytetään
        //ne joko vähentävät tai plussaavat
        //jostain syystä tehdään ylimääräinen f kutsu joka suurentaa x ennen kuin 
        //kutsutaan oikeasti (foon sisällä for no reason) :(