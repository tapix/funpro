function anonymReturn(){
    //palauttaa funktion jolla voi vertailla kahta lukua (tai merkkijonoa)
    return (x,y) => { return x.toString().localeCompare(y.toString()) };
}

function resultGen(x, y, z){
    //Käyttö tehtävänannossa: 2015v lämpötilataulukko,2016v lämpötilataulukko, niille haluttu funktion.
    //luo annetun arvon perusteella tulosstringgin
    let counting=0;
    if(y.length===x.length){
        for(var i=0;i<y.length;i++){
              var temp=z(y[i],x[i]); //verrataan 2016 vs 2015
              if(temp===1){
                  counting=counting+temp;
              }
        }
    }else{
        console.log("Annetut taulukot eripituisia.")
    }
    return "Vuoden 2016 keskilämpötila oli korkeampi " + counting + " kuukautena.";
}
let testing = anonymReturn();
console.log(resultGen([25,26,26,27,23,23],[20,23,25,28,25,22],testing));
console.log(resultGen([25,26,26,27,23,23],[28,28,28,28,28,28],testing));
console.log(resultGen([25,26,26,27,23,23],[28,28,28,28,28],testing));
console.log(resultGen([28,28,28,28,28,28],[25,26,26,27,23,23],testing));
//console.log(resultGen(testing,,);
//console.log(resultGen(testing,,);