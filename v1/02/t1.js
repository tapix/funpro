function anonymReturn(){
    //palauttaa funktion jolla voi vertailla kahta lukua (tai merkkijonoa)
    return (x,y) => { return x.toString().localeCompare(y.toString()) };
}
function resultGen(x){
    //luo annetun arvon perusteella tulosstringgin
    let tempstring;
    tempstring="Paluuarvo "+x+": ";
    if(x===1){
        tempstring=tempstring+"ensimmäinen on suurempi.";
    }else if(x===-1){
        tempstring=tempstring+"toinen on suurempi.";
    }else if(x===0){
        tempstring=tempstring+"luvut ovat yhtä suuria.";
    }else{
        tempstring=tempstring+"jotain meni pieleen.";
    }
    return tempstring;
}
let testing = anonymReturn();
console.log(resultGen(testing(4,4)));
console.log(resultGen(testing(3,6)));
console.log(resultGen(testing(6,3)));