;tehtävä 1 -----------------------------------------------------
(ns test_lein.core
  (:gen-class))
(def ekavuosi [-3,-5,-1,2,6,11,13,12,8,8,5,-1])
(def tokavuosi [-2,-8,-5,3,8,13,16,15,10,10,7,1])


(defn positives 
     ([mean] (filter #(> % 0)mean))
)
(defn meantemp
  "averagetemps"
      ([meantemps](/ (reduce + meantemps) (count meantemps)))
)

(defn formatter
  "Makes the value to 0.00 format"
  ([meantemperature]
     (format "%.2f" (double  meantemperature))
    )
)

(defn average
  "average"
  ([ekav tokav]
    (map #(/ % 2)
         (map + ekav tokav)
    )
  )
)

(def keskiarvo (average ekavuosi tokavuosi))
(def PosKeskiarvo (formatter (meantemp (positives keskiarvo))))
(defn -main
  "kuvaus"                                                                                     
  [& args]
    (println "Keskiarvo: " keskiarvo)
    (println "PosKeskiarvot keskilämpötila: " PosKeskiarvo) 
    )

  
  
  
;tehtävä 2 -----------------------------------------------------

(ns test_lein.core
  (:gen-class))

 (def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])

(defn nesteformonth
  "counts the given liquid for the given month in the given table"
  ([table month liquid]
        (map liquid table
          (filter #(= (:kk %) month) table
          )
        )
  )
)
(defn discount
    ([table table2]
      (map - table table2)
    )
)

(defn total
    ([table]
      (reduce + table)
    )
)
(defn formatter
  "Makes the value to 0.00 format"
  ([meantemperature]
     (format "%.2f" (double  meantemperature))
    )
)
(def nestehuhtikuu (nesteformonth food-journal 4 :neste))
(def vesihuhtikuu (nesteformonth food-journal 4 :vesi))
(def nesteilmanvesihuhtikuu (discount nestehuhtikuu vesihuhtikuu))
(defn -main
  "kuvaus"                                                                                     
  [& args]
    (println "Yhteensä: " (formatter (total nesteilmanvesihuhtikuu)))
    )
  
  
  
;tehtävä 3 -----------------------------------------------------


  
  